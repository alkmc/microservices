module gitlab.com/32b3/microservices/currency

go 1.15

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/hashicorp/go-hclog v0.14.1
	github.com/mattn/go-colorable v0.1.8 // indirect
	golang.org/x/net v0.0.0-20201027133719-8eef5233e2a1 // indirect
	golang.org/x/sys v0.0.0-20201028094953-708e7fb298ac // indirect
	golang.org/x/text v0.3.4 // indirect
	google.golang.org/genproto v0.0.0-20201026171402-d4b8fe4fd877
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
)
