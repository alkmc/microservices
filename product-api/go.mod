module gitlab.com/32b3/microservices/product-api

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/go-openapi/analysis v0.19.11 // indirect
	github.com/go-openapi/errors v0.19.8 // indirect
	github.com/go-openapi/runtime v0.19.23
	github.com/go-openapi/spec v0.19.11 // indirect
	github.com/go-openapi/strfmt v0.19.7 // indirect
	github.com/go-openapi/swag v0.19.11 // indirect
	github.com/go-openapi/validate v0.19.12 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-hclog v0.14.1
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/nicholasjackson/env v0.6.0
	github.com/stretchr/testify v1.6.1
	github.com/tidwall/pretty v1.0.2 // indirect
	gitlab.com/32b3/microservices/currency v0.0.0-20201021170030-94b13c832caf
	go.mongodb.org/mongo-driver v1.4.2 // indirect
	golang.org/x/net v0.0.0-20201027133719-8eef5233e2a1 // indirect
	golang.org/x/sys v0.0.0-20201028094953-708e7fb298ac // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20201026171402-d4b8fe4fd877 // indirect
	google.golang.org/grpc v1.33.1
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
